# worker.py

import json
import os

import redis
import base64
import hashlib

REDIS_HOST = os.getenv('REDIS_HOST')

# Function to perform a hash-based computation on the given buffer
def work(buffer, iterations):
    output = hashlib.sha512(buffer).digest()
    for i in range(iterations - 1):
        output = hashlib.sha512(output).digest()
    return output


# Entry point of the worker script
def entry():
    connection = redis.Redis(host=REDIS_HOST)

    while True:
        # Retrieve a job from the 'queue:jobs' Redis list
        packed = connection.blpop(['queue:jobs'], 20)

        if not packed:
            continue

        # Unpack the job data
        job = json.loads(packed[1])
        job_data = base64.decodebytes(job['data'].encode('utf-8'))

        # Perform the hash computation on the job data
        hashed_value = work(job_data, int(job['iterations']))

        # Prepare the completed job object
        completed_job = {
            'id': job['id'],
            'data': base64.encodebytes(hashed_value).decode("utf-8"),
        }

        # Print the completed job and push it to the 'list:completed-jobs' Redis list
        print(completed_job)
        connection.lpush('list:completed-jobs', json.dumps(completed_job))


# Run the worker script
entry()

# import necessary modules
import json
import os
from flask import Flask, request
import uuid
import redis
import base64

# initialize Flask app
app = Flask(__name__)

# get Redis host from environment variable
REDIS_HOST = os.getenv('REDIS_HOST')

# establish connection to Redis
connection = redis.Redis(host=REDIS_HOST)

# endpoint for enqueueing a job
@app.route("/enqueue", methods=['PUT'])
def enqueue():
    # extract iterations and data from request arguments
    iterations = request.args.get('iterations')
    id = str(uuid.uuid4())
    data = request.data

    # create a job dictionary
    job = {
        'id': id,
        'data': base64.encodebytes(data).decode("utf-8"),
        'iterations': iterations
    }

    # push the job to the jobs queue in Redis
    connection.rpush('queue:jobs', json.dumps(job))

    return id

# endpoint for pulling completed jobs
@app.route("/pullCompleted", methods=['POST'])
def pullCompleted():
    # extract the number of top completed jobs to retrieve
    top = int(request.args.get('top'))

    # get the top completed jobs from the completed jobs list in Redis
    top_completed_jobs = connection.lrange('list:completed-jobs', 0, top)
    parsed_jobs = []

    # parse each completed job and decode the job data
    for completed_job in top_completed_jobs:
        completed_job = json.loads(completed_job)
        job_data = base64.decodebytes(completed_job['data'].encode('utf-8'))
        parsed_jobs.append({
            'id': completed_job['id'],
            'data': job_data
        })

    return str(parsed_jobs)

# copy worker.py to the remote server
scp -i $2 -o "StrictHostKeyChecking=no" -o "ConnectionAttempts=60" /home/ubuntu/worker.py ubuntu@$1:/home/ubuntu/worker.py

# execute commands on the remote server
ssh -i $2 -o "StrictHostKeyChecking=no" -o "ConnectionAttempts=10" ubuntu@$1 <<EOF
    # set the Redis host environment variable
    export REDIS_HOST=$REDIS_HOST

    # add the Redis host environment variable to the user's bashrc file
    echo REDIS_HOST=$REDIS_HOST >> ~/.bashrc

    # update packages
    sudo apt update

    # install Python RQ library
    sudo apt install python3-rq -y

    # run the worker.py script in the background and redirect output to null
    python3 worker.py &>/dev/null &

    # exit the SSH session
    exit
EOF
